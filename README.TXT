Product Importer, Exporter AND Sincronization by CSV File for OpenCart 1.5.X | Version 1.2014.08.13
================================================
About
=======
The product importer is an back-office tool that allows the importation/update of a list of products, that are in a csv list file format like this:

Checkout video at: http://youtu.be/Zgv0_ryteug

name,model,price,quantity,image_url,product_description,sku,upc,weight,category_name,attribute1,attribute2,
usb pen,usb pen,7,400,http://www.joelcorreia.pt/product_importer/pen.jpg,<b>usb pen 5 gb</b> <br>,0002,0002,0.2,PC,text attribute 1,text attribute 2,


Examples of some situation(USE CASES):
=======
==CASE 1
==CSV File:
name,model,price,quantity,image_url,product_description,sku,upc,weight,category_name,attribute1,attribute2,
usb pen,,8,,,,,,,,,,
==Situation:
Already exists a product in opencart with the name: usb pen
==Behavior:
Updates the price to 8

=======
==CASE 2
==CSV File:
name,model,price,quantity,image_url,product_description,sku,upc,weight,category_name,attribute1,attribute2,
usb pen,usb pen,7,400,http://www.joelcorreia.pt/product_importer/pen.jpg,<b>usb pen 5 gb</b> <br>,0002,0002,0.2,PC,text attribute 1,,
==Situation:
A product with the model "usb pen" does not exists in open cart
==Behavior:
creates a new product, with the name:"usb pen" and price 7 and with the rest of the fields


This module download the image of each product from an url and creates an new product, the other fields that are not present in the csv file are filled by referring to an existing product.
By default if the name of the product already exists in opencart, it will update the product fields. These feature can be unselect.

The columns in the csv file must be in the correct order.

Install
=======
1) copy admin folders in the root folder of your website(opencart instalation folder)
2) if You have any additional languages installed, you must also copy the contents of the "language"-folder to your appropriate other language(s).

 eg:
	..admin/language/english/module/products_importer.php    copy to    ..admin/language/YOUROTHERLANGUAGE/module/products_importer.php
	..catalog/language/english/module/products_importer.php    copy to    ..catalog/language/YOUROTHERLANGUAGE/module/products_importer.php

Checkout video at: http://youtu.be/Zgv0_ryteug

Config
======
In admin section go to Extension/Modules and config the Product Importer, Exporter AND Sincronization by CSV File module

Limitations
=======
The column image_url, might slow down the process of importation/synchronization of products, these links of new images that don�t already exists in opencart image dir makes the process importation/synchronization very slow.
So i don�t recommend the usage of the importer as an "image downloader", it is recommend to have only 15 new images (links) per csv file.( 15 images might take 1 minute to download)

The importation of a larger csv file, might be risk, if in some columns are special characters ( , " /), this happen often in product description column.

This extension is limited to 10 fields/columns(model,name,quantity,price,image_url,product_description,sku,upc,weight,category_name), it has to exist at least one product in opencart to be the reference product, for the other columns

Supports only 1 language, but tests made in installation with two languages worked ok.

It does not support fields values that contains the csv special characters( , " /)

It associates the products to the first category(only one) found by the category name given.

Changelog
======
1.2014.08.13
 * added sort_order to product_image
1.2013.07.20
 * fix for 1.5.5.1, when updating keep the information like (option, related, etc) 
1.2013.01.24
 * MAX_FILE_SIZE define to 4MB in the products_importer.tpl
Version 1.2012.12.24
 * add the option to force the UTF character importation

Version 1.2012.09.29
 * add support for tag an url_alias(seo)

Version 1.2012.08.30
 * bug fixed: when updating products, if category field is blank it keeps the original categories

Version 1.2012.06.22
 * in some versions of php there was an malfunction, bug fixed when count number of row of an csv file

Version 1.2012.05.28
 * due to the differences of the fgetcsv function in php 5.2 and 5.3 escape char was disable
 * extra validation of the number of rows

Version 1.2012.03.17
 * some minor fixes on the export and product updated inserted counter

Version 1.2012.03.09
 * improvements on the export function, remove ( , " /) characters from the field values

Version 1.2012.03.03
 * added product attribute import and export option

Version 1.2012.01.13
 * new improvements on the downloading of images
 * added an execution process time limit
 * added option for the creation of new products

Version 1.2012.01.11
 * improved error handling when downloading images

Version 1.2011.12.18
 * columns order has changed
 * added distinction between name and model
 * possibility to choose the identifier field for existing products(name or model or sku), default is name
 * bug fixes on the exporter

Version 1.2011.11.28
 * added possibility to choose the identifier field for existing products(model or sku)

Version 1.2011.11.06
 * exportation function added
 * product_description is not required, when updating it keep the previous information store, in case of an empty descrition column
 * change default delimiter ; to ,
Version 1.2011.11.03
 * design change
 * add possible defenition of: delimiter, enclosure, escape
 * add support for category name column in csv file, if category name is found it will use it, if not it will use the select one in the categogy combobox

Version 1.2011.09.23
 * supports product update, for example: it allows to select if you want to update product fields if the Model/Name exists in OpenCart
 * when sincronize/update is select if there is any blank it will not update that field, the product description is an mandatory field

Version 1.2011.09.16
 * support images links that are encode in html characters, for example converts "%7B" to "{"

Version 1.2011.09.15
 * support some special caracters like: � � �, and html codes

Version 1.2011.09.10
 * uses the image folder to upload the csv temporary file
 * in case of error prevents from inserting blank products

Version 1.2011.09.08
 * add fields: description,sku, upc and weight
 * in case of an equal image name exists in the image folder, it uses the existing image


Version 1.2011.09.02
 * initial release

Credits
========
#  Original developed to OpenCart 1.5.X by  Joel Correia, web: www.joelcorreia.pt email: email@joelcorreia.pt