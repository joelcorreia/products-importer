<?php

#############################################################################
#	Version 1.2014.08.13
#  	Original developed to OpenCart 1.5.x by  Joel Correia, web: www.joelcorreia.com email: email@joelcorreia.com
#
#############################################################################
class ControllerModuleproductsimporter extends Controller {
	private $error = array();

	public function index()
	{

		$this->load->language('module/products_importer');
		//$this->language->load('module/products_importer'); 1.5.5 bug
		$this->document->setTitle = $this->language->get('heading_title');

		$this->load->model('setting/setting');

		$this->data['observations'] = "";

		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

    	$this->load->model('catalog/category');
		$this->data['categories'] = $this->model_catalog_category->getCategories(0);

		$this->data['text_none'] =  $this->language->get('text_none');
		$this->data['default_category_title'] =  $this->language->get('default_category_title');
		$this->data['default_tax_title'] =  $this->language->get('default_tax_title');
		$this->data['default_product_title'] =  $this->language->get('default_product_title');
		$this->data['default_image_download_title'] =  $this->language->get('default_image_download_title');
		$this->data['update_products_if_model_text_equal'] =  $this->language->get('update_products_if_model_text_equal');
		$this->data['insert_products_if_new'] =  $this->language->get('insert_products_if_new');
		$this->data['time_execution_limit'] =  $this->language->get('time_execution_limit');

		$this->data['text_csv_file_example_link'] =  $this->language->get('text_csv_file_example_link');
		$this->data['text_csv_head_example'] =  $this->language->get('text_csv_head_example');
		$this->data['text_csv_row_example'] =  $this->language->get('text_csv_row_example');
		$this->data['text_csv_example_title'] =  $this->language->get('text_csv_example_title');

		$this->data['text_product_row_options'] =  $this->language->get('text_product_row_options');
		$this->data['text_compare_product_field'] =  $this->language->get('text_compare_product_field');

		$this->data['text_settings'] =  $this->language->get('text_settings');
		$this->data['text_delimiter'] =  $this->language->get('text_delimiter');
		$this->data['text_enclosure'] =  $this->language->get('text_enclosure');
		$this->data['text_escape'] =  $this->language->get('text_escape');
		$this->data['text_execute'] =  $this->language->get('text_execute');
		$this->data['text_force_utf'] =  $this->language->get('text_force_utf');

		$this->data['text_import_or_sincronize_title'] =  $this->language->get('text_import_or_sincronize_title');
		$this->data['text_export_title'] =  $this->language->get('text_export_title');
		$this->data['text_download_csv_title'] =  $this->language->get('text_download_csv_title');
		$this->data['text_download_csv_observations'] =  $this->language->get('text_download_csv_observations');

		$this->data['text_product_row_attributes'] =  $this->language->get('text_product_row_attributes');
		$this->data['text_product_row_attribute1_title'] =  $this->language->get('text_product_row_attribute1_title');
		$this->data['text_product_row_attribute2_title'] =  $this->language->get('text_product_row_attribute2_title');


		$this->load->model('catalog/product');
		$this->data['products'] = $this->model_catalog_product->getProducts(0);

		$this->data['product_id'] =  0;
		$this->data['tax_class_id'] =  0;
		$this->data['category_id'] =  0;

		$observations0 = "";


		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate()))
		{
			$this->model_setting_setting->editSetting('products_importer', $this->request->post);
			$this->load->model('catalog/category');


			if (isset($this->request->post['tax_class_id']))
				$this->data['tax_class_id'] = $this->request->post['tax_class_id'];

			if (isset($this->request->post['category_id']))
				$this->data['category_id'] = $this->request->post['category_id'];

			if (isset($this->request->post['product_id']))
				$this->data['product_id'] = $this->request->post['product_id'];

			if (isset($this->request->post['update_products']))
				$update_products = true;
			else
				$update_products = false;

			if (isset($this->request->post['force_utf']))
				$force_utf = true;
			else
				$force_utf = false;

			if (isset($this->request->post['insert_products']))
				$insert_products = true;
			else
				$insert_products = false;

				$target_path = DIR_IMAGE;
				$target_path = $target_path . basename( $_FILES['uploadedfile']['name']);
				if (is_uploaded_file($_FILES['uploadedfile']['tmp_name']))
				{
				if(move_uploaded_file($_FILES['uploadedfile']['tmp_name'], $target_path))
				{
					ini_set('auto_detect_line_endings', true);
					//setlocale(LC_ALL, "en_US.UTF-8");
					//setlocale(LC_CTYPE, "en.UTF16");

					$linecount = 0;
					$handle_test = fopen($target_path, "r");
					while(!feof($handle_test)){
					  $line = fgets($handle_test, 4096);
					  $linecount = $linecount + substr_count($line, PHP_EOL);
					}
					fclose($handle_test);
					if($linecount==0)
					{
						$handle_test = fopen($target_path, "r");
						while(!feof($handle_test)){
						  $line = fgets($handle_test, 4096);
						  $linecount = $linecount + substr_count($line, "\n");
						}
						fclose($handle_test);
					}
					if($linecount==0)
					{
						$handle_test = fopen($target_path, "r");
						while(!feof($handle_test)){
						  $line = fgets($handle_test, 4096);
						  $linecount = $linecount + substr_count($line, "\r\n");
						}
						fclose($handle_test);
					}

					//ini_set('display_warnings', false);
					$handle = fopen($target_path, "r");
					$seconds_rows = false;
					$number_of_cvs_products = -1;
					$number_of_new_products = 0;
					$number_of_update_products = 0;

					$delimiter = ',';
					$enclosure = '"';
					$escape = '\\';

					if (isset($this->request->post['delimiter']))
				 		$delimiter = $this->request->post['delimiter'];

				 	if (isset($this->request->post['enclosure']))
				 		$enclosure = $this->request->post['enclosure'];

				 	if (isset($this->request->post['escape']))
				 		$escape = $this->request->post['escape'];


				 	if (isset($this->request->post['time_execution_limit']))
				 	{
				 	 if (is_numeric($this->request->post['time_execution_limit']))
				 			$time_limit = $this->request->post['time_execution_limit'];
				 		else
				 			$time_limit = 60;
				 	}
				 	else
						$time_limit = 60; //seconds


					$attribute1_name = '';
					$attribute1_id = 0;
				 	if (isset($this->request->post['attribute1_name']))
				 			$attribute1_name = $this->request->post['attribute1_name'];

					$attribute2_name = '';
					$attribute2_id = 0;
				 	if (isset($this->request->post['attribute2_name']))
				 			$attribute2_name = $this->request->post['attribute2_name'];

				 	$language_id_result0 = $this->db->query("SELECT language_id FROM " . DB_PREFIX . "language WHERE `code`=(SELECT CAST(`value` AS CHAR(10000) CHARACTER SET utf8) FROM " . DB_PREFIX . "setting WHERE `key`='config_language' LIMIT 1)");
					$language_id0 = $language_id_result0->row['language_id'];

					$this->load->model('localisation/language');
					$this->data['languages'] = $this->model_localisation_language->getLanguages();

				 	if($attribute1_name <> '' || $attribute2_name <> '')
				 	{
						if($attribute1_name <> '')
						{
							$data_attribute0['attribute_group_id'] = 0;
						 	$results0 = $this->db->query("SELECT IFNULL(attribute_group_id,0) as attribute_group_id  FROM " . DB_PREFIX . "attribute_group_description WHERE  name='" . mysql_real_escape_string($attribute1_name) . "' AND language_id=" . mysql_real_escape_string($language_id0) . " UNION SELECT 0 LIMIT 1");
						 	if($results0->row['attribute_group_id']==0)
						 	{
						 		$this->load->model('catalog/attribute_group');
						 		$data_attribute_group0['sort_order'] = 0;
							 	foreach ($this->data['languages'] as $language)
								{
										$data_attribute_group0['attribute_group_description'][$language['language_id']] =
										array(
											'name'             => $attribute1_name
										);
								}
								$this->model_catalog_attribute_group->addAttributeGroup($data_attribute_group0);
								$results01 = $this->db->query("SELECT IFNULL(attribute_group_id,0) as attribute_group_id  FROM " . DB_PREFIX . "attribute_group_description WHERE  name='" . mysql_real_escape_string($attribute1_name) . "' AND language_id=" . mysql_real_escape_string($language_id0) . " UNION SELECT 0 LIMIT 1");
								$data_attribute0['attribute_group_id'] = $results01->row['attribute_group_id'];
						 	}
						 	else
						 		$data_attribute0['attribute_group_id'] = $results0->row['attribute_group_id'];


						 	$results1 = $this->db->query("SELECT IFNULL(attribute_id,0) as attribute_id  FROM " . DB_PREFIX . "attribute_description WHERE  name='" . mysql_real_escape_string($attribute1_name) . "' AND language_id=" . mysql_real_escape_string($language_id0) . " UNION SELECT 0 LIMIT 1");
						 	if($results1->row['attribute_id']==0)
						 	{
						 		$this->load->model('catalog/attribute');
								$data_attribute0['attribute_group_id'] = $data_attribute0['attribute_group_id'];
								$data_attribute0['sort_order'] = 0;
						 		foreach ($this->data['languages'] as $language)
								{
										$data_attribute0['attribute_description'][$language['language_id']] =
										array(
											'name'             => $attribute1_name
										);
								}
			      				$this->model_catalog_attribute->addAttribute($data_attribute0);
			      				$results11 = $this->db->query("SELECT IFNULL(attribute_id,0) as attribute_id  FROM " . DB_PREFIX . "attribute_description WHERE  name='" . mysql_real_escape_string($attribute1_name) . "' AND language_id=" . mysql_real_escape_string($language_id0) . " UNION SELECT 0 LIMIT 1");
			      				$attribute1_id = $results11->row['attribute_id'];
						 	}
						 	else
						 		$attribute1_id = $results1->row['attribute_id'];

					 	}
				 		if($attribute2_name <> '')
						{
							$data_attribute0['attribute_group_id'] = 0;
						 	$results0 = $this->db->query("SELECT IFNULL(attribute_group_id,0) as attribute_group_id  FROM " . DB_PREFIX . "attribute_group_description WHERE  name='" . mysql_real_escape_string($attribute2_name) . "' AND language_id=" . mysql_real_escape_string($language_id0) . " UNION SELECT 0 LIMIT 1");
						 	if($results0->row['attribute_group_id']==0)
						 	{
						 		$this->load->model('catalog/attribute_group');
						 		$data_attribute_group0['sort_order'] = 0;
							 	foreach ($this->data['languages'] as $language)
								{
										$data_attribute_group0['attribute_group_description'][$language['language_id']] =
										array(
											'name'             => $attribute2_name
										);
								}
								$this->model_catalog_attribute_group->addAttributeGroup($data_attribute_group0);
								$results01 = $this->db->query("SELECT IFNULL(attribute_group_id,0) as attribute_group_id  FROM " . DB_PREFIX . "attribute_group_description WHERE  name='" . mysql_real_escape_string($attribute2_name) . "' AND language_id=" . mysql_real_escape_string($language_id0) . " UNION SELECT 0 LIMIT 1");
								$data_attribute0['attribute_group_id'] = $results01->row['attribute_group_id'];
						 	}
						 	else
						 		$data_attribute0['attribute_group_id'] = $results0->row['attribute_group_id'];


						 	$results1 = $this->db->query("SELECT IFNULL(attribute_id,0) as attribute_id  FROM " . DB_PREFIX . "attribute_description WHERE  name='" . mysql_real_escape_string($attribute2_name) . "' AND language_id=" . mysql_real_escape_string($language_id0) . " UNION SELECT 0 LIMIT 1");
						 	if($results1->row['attribute_id']==0)
						 	{
						 		$this->load->model('catalog/attribute');
								$data_attribute0['attribute_group_id'] = $data_attribute0['attribute_group_id'];
								$data_attribute0['sort_order'] = 0;
						 		foreach ($this->data['languages'] as $language)
								{
										$data_attribute0['attribute_description'][$language['language_id']] =
										array(
											'name'             => $attribute2_name
										);
								}
			      				$this->model_catalog_attribute->addAttribute($data_attribute0);
			      				$results11 = $this->db->query("SELECT IFNULL(attribute_id,0) as attribute_id  FROM " . DB_PREFIX . "attribute_description WHERE  name='" . mysql_real_escape_string($attribute2_name) . "' AND language_id=" . mysql_real_escape_string($language_id0) . " UNION SELECT 0 LIMIT 1");
			      				$attribute2_id = $results11->row['attribute_id'];
						 	}
						 	else
						 		$attribute2_id = $results1->row['attribute_id'];

					 	}
					}

			 		$start_time=microtime();
					$start_time=explode(" ",$start_time);
					$start_time=$start_time[1]+$start_time[0];

					$end_time=microtime();
					$end_time=explode(" ",$end_time);
					$end_time=$end_time[1]+$end_time[0];

					//$observations0 = "Log: ";
					//If using PHP above 5.3 and want to use the escape
					//while ((($produto_csv0 = fgetcsv($handle, 0,$delimiter[0],$enclosure[0],$escape[0])) != FALSE) AND (($end_time-$start_time)<$time_limit) AND ($number_of_cvs_products<$linecount))

					while ((($produto_csv0 = fgetcsv($handle, 0,$delimiter[0],$enclosure[0])) != FALSE) AND (($end_time-$start_time)<$time_limit) AND ($number_of_cvs_products<$linecount))
					{
						//$observations0 = sprintf("%s - %d",$observations0,$end_time-$start_time);
						$end_time=microtime();
						$end_time=explode(" ",$end_time);
						$end_time=$end_time[1]+$end_time[0];

						$number_of_cvs_products++;
						if($seconds_rows)
						{



							$name0 =  $force_utf ? utf8_encode($produto_csv0[0]) : $produto_csv0[0];//mb_convert_encoding($produto_csv0[0], "ISO-8859-1", "UTF-8");
							$model0 = $force_utf ? utf8_encode($produto_csv0[1]) : $produto_csv0[1];
							$price0 = $produto_csv0[2];
							$quantity0 = $produto_csv0[3];
							$image_url0 = urldecode($produto_csv0[4]);
							$product_description0 = $force_utf ? utf8_encode($produto_csv0[5]) : $produto_csv0[5];
							$sku0 = $force_utf ? utf8_encode($produto_csv0[6]) : $produto_csv0[6];
							$upc0 = $force_utf ? utf8_encode($produto_csv0[7]) : $produto_csv0[7];
							$weight0 = $produto_csv0[8];
							$category_name0 = $produto_csv0[9];

							$attribute10 = ($attribute1_name == '' ? '' :  ($force_utf ? utf8_encode($produto_csv0[10]) : $produto_csv0[10]));
							$attribute20 = ($attribute2_name == '' ? '' :  ($force_utf ? utf8_encode($produto_csv0[11]) : $produto_csv0[11]));


							if (isset($this->request->post['compare_product_field']))
				 				$compare_product_field = $this->request->post['compare_product_field'];
				 			else
				 				$compare_product_field = 'model';

				 			//name
				 			$sql_string = "SELECT IFNULL(product_id,0) product_id FROM `" . DB_PREFIX . "product_description` WHERE name = '" . mysql_real_escape_string($name0) . "' AND language_id=" . $language_id0 . " LIMIT 1 UNION SELECT 0";

				 			switch ($compare_product_field)
				 			{
				 				case 'name': ;break;
				 				case 'model': $sql_string = ("SELECT IFNULL(product_id,0) product_id FROM " . DB_PREFIX . "product WHERE model = '" . mysql_real_escape_string($model0) . "' LIMIT 1 UNION SELECT 0");break;
				 				case 'sku': $sql_string = ("SELECT IFNULL(product_id,0) product_id FROM " . DB_PREFIX . "product WHERE sku = '" . mysql_real_escape_string($sku0) . "' LIMIT 1 UNION SELECT 0");break;
				 			}

							$query_product_exists0 = $this->db->query($sql_string);


							$product_id0 = $query_product_exists0->row['product_id'];
							if($product_id0>0 && $update_products)
							{

								$data_new_product0 = $this->model_catalog_product->getProduct($product_id0);

								//category
								if($category_name0=='')
								{
									$data_new_product0['product_category'] = $this->model_catalog_product->getProductCategories($product_id0);
								}
								else
								{

									$query_category_exists0 = $this->db->query("SELECT IFNULL(category_id,0) category_id FROM " . DB_PREFIX . "category_description WHERE name = '" . mysql_real_escape_string($category_name0) . "' LIMIT 1 UNION SELECT 0");
									$category_id0 = $query_category_exists0->row['category_id'];
									$data_new_product0['product_category'] = ($category_id0>0) ? array($category_id0) : array($this->data['category_id']);
								}
							}
							else
							{

								$data_new_product0 = $this->model_catalog_product->getProduct($this->data['product_id']);

								//category
								if($category_name0!='')
								{
									$query_category_exists0 = $this->db->query("SELECT IFNULL(category_id,0) category_id FROM " . DB_PREFIX . "category_description WHERE name = '" . mysql_real_escape_string($category_name0) . "' LIMIT 1 UNION SELECT 0");
									$category_id0 = $query_category_exists0->row['category_id'];
									$data_new_product0['product_category'] = ($category_id0>0) ? array($category_id0) : array($this->data['category_id']);
								}
							}



							if($model0!='')
								$data_new_product0['model'] = $model0;
							else
								$model0 = $data_new_product0['model']; // $model0=$name0

							//if($name0=='' && $compare_product_field='name')
							//	$name0 = $model0; //new product with name=model will be created

							if($quantity0!='')
								$data_new_product0['quantity'] = $quantity0;

							if($price0!='')
								$data_new_product0['price'] = $price0;

							if($sku0!='')
								$data_new_product0['sku'] = $sku0;

							if($upc0!='')
								$data_new_product0['upc'] = $upc0;

							if($weight0!='')
								$data_new_product0['weight'] = $weight0;



							foreach ($this->data['languages'] as $language)
							{
								$product_meta_keyword0 = $product_description0;
								$product_meta_description0 = $product_description0;
								$product_tag0 = $product_description0;

								if($product_id0>0 && $update_products)
								{

									if(version_compare(VERSION, '1.5.4', '>')) {
									    $results0 = $this->db->query("SELECT IFNULL(name,'') name,IFNULL(description,'') description,IFNULL(meta_keyword,'') meta_keyword,IFNULL(meta_description,'') meta_description,IFNULL(tag,'') tag  FROM " . DB_PREFIX . "product_description WHERE product_id = " . mysql_real_escape_string($product_id0) . " AND language_id=" . mysql_real_escape_string($language['language_id']) . " UNION SELECT '','','','','' LIMIT 1");
									} else {
									    $results0 = $this->db->query("SELECT IFNULL(name,'') name,IFNULL(description,'') description,IFNULL(meta_keyword,'') meta_keyword,IFNULL(meta_description,'') meta_description  FROM " . DB_PREFIX . "product_description WHERE product_id = " . mysql_real_escape_string($product_id0) . " AND language_id=" . mysql_real_escape_string($language['language_id']) . " UNION SELECT '','','','' LIMIT 1");
									}


									if($name0=='')
										$name0 = $results0->row['name'];

									if($product_description0=='')
									{
										$product_description0 = $results0->row['description'];
										$product_meta_keyword0 = $results0->row['meta_keyword'];
										$product_meta_description0 = $results0->row['meta_description'];
										$product_tag0 = isset($results0->row['tag']) ? $results0->row['tag'] : $results0->row['meta_keyword'];
									}

								}
								else
								{
									if($product_description0=='') //required field in case of new product
									{
										$product_description0 = $name0;
										$product_meta_keyword0 = $name0;
										$product_meta_description0 = $name0;
										$product_tag0 = $name0;
									}
								}

								if($name0=='')
									$name0 = $model0;

								$data_new_product0['product_description'][$language['language_id']] =
								array(
									'name'             => $name0,
									'description'      => $product_description0,
									'meta_keyword'     => $product_meta_keyword0,
									'meta_description' => $product_meta_description0,
									'tag' => $product_tag0
								);
							}

							//url_alias
							if (!($product_id0>0 && $update_products))
								$data_new_product0['keyword'] = $this->generateSlug($name0);


							$data_new_product0['product_tag'] = array($name0);
							$data_new_product0['tax_class_id'] = $this->data['tax_class_id'];
							$data_new_product0['product_store'] = array(0);


							$image_name0 = basename($image_url0);



							if (!file_exists(DIR_IMAGE . $image_name0))
							{

									//try {
											if($this->request->post['image_download'] == 'allow_url_fopen')
											{
												@file_put_contents(DIR_IMAGE . $image_name0 , @file_get_contents($image_url0));

												if (file_exists(DIR_IMAGE . $image_name0))
												{
												   if(!getimagesize(DIR_IMAGE . $image_name0))
													unlink(DIR_IMAGE . $image_name0);
												}
												else
													$image_name0='no_image.jpg';
											}
											else
											{
												$ch = @curl_init($image_url0);
												$fp = fopen(DIR_IMAGE . $image_name0, 'wb');
												curl_setopt($ch, CURLOPT_FILE, $fp);
												curl_setopt($ch, CURLOPT_HEADER, 0);
												//curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
												//curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
												@curl_exec($ch);

										    	if (file_exists(DIR_IMAGE . $image_name0))
												{
												   if(!@getimagesize(DIR_IMAGE . $image_name0))
													unlink(DIR_IMAGE . $image_name0);
												}
												else
													$image_name0='no_image.jpg';

												curl_close($ch);
												fclose($fp);
											}
										/*} catch (Exception $e) {

											if (file_exists(DIR_IMAGE . $image_name0))
											{
											   if(!getimagesize(DIR_IMAGE . $image_name0))
												unlink(DIR_IMAGE . $image_name0);
											}

											$image_name0='no_image.jpg';
										}
										*/
							}
							//else
							//	$image_name0 = 'no_image.jpg';


							if (!file_exists(DIR_IMAGE . $image_name0))
								$image_name0!='';

							//$observations0 = sprintf("%s -%s:%s",$observations0,$image_name0,file_exists(DIR_IMAGE . $image_name0));

							if($image_name0!='')
							{
								$data_new_product0['image'] =  $image_name0;
								$data_new_product0['product_image'][] = array(
											'image'   =>  $image_name0,
											'sort_order' => 0
								);
							}

							//attributes
							if(($attribute1_id >0 && $attribute10<>'') || ($attribute2_id >0 && $attribute20<>''))
							{
								$data_new_product0['product_attribute'] = array();
								$attribute_aux_i =0;
								if($attribute1_id >0 && $attribute10<>'')
								{
									$data_new_product0['product_attribute'][] =	array(
														'attribute_id'   =>  $attribute1_id,
														'product_attribute_description' => array()
													);
									//foreach ($this->data['languages'] as $language)
									//{
										$data_new_product0['product_attribute'][$attribute_aux_i]['product_attribute_description'][$language_id0]= array('text'   =>  $attribute10);
									//}
									$attribute_aux_i++;
								}

								if($attribute2_id >0 && $attribute20<>'')
								{
								$data_new_product0['product_attribute'][] =	array(
														'attribute_id'   =>  $attribute2_id,
														'product_attribute_description' => array()
													);
									//foreach ($this->data['languages'] as $language)
									//{
										$data_new_product0['product_attribute'][$attribute_aux_i]['product_attribute_description'][$language_id0]= array('text'   =>  $attribute20);
									//}
								}
							}




							if(strlen($data_new_product0['model']) > 0)
							{
								if($product_id0>0 && $update_products)
								{
									$number_of_update_products++;

									$data_new_product0['product_option'] = $this->model_catalog_product->getProductOptions($product_id0);
									$data_new_product0['product_attribute'] = $this->model_catalog_product->getProductAttributes($product_id0);
									$data_new_product0['product_discount'] = $this->model_catalog_product->getProductDiscounts($product_id0);
									$data_new_product0['product_special'] = $this->model_catalog_product->getProductSpecials($product_id0);
									$data_new_product0['product_image'] = $this->model_catalog_product->getProductImages($product_id0);
									$data_new_product0['product_download'] = $this->model_catalog_product->getProductDownloads($product_id0);
									$data_new_product0['product_category'] = $this->model_catalog_product->getProductCategories($product_id0);
									$data_new_product0['product_filter'] = $this->model_catalog_product->getProductFilters($product_id0);

									$data_new_product0['product_related'] = $this->model_catalog_product->getProductRelated($product_id0);
									$data_new_product0['product_reward'] = $this->model_catalog_product->getProductRewards($product_id0);
									$data_new_product0['product_layout'] = $this->model_catalog_product->getProductLayouts($product_id0);
									
									$this->model_catalog_product->editProduct($product_id0, $data_new_product0);
								}
								else
								{
									if($insert_products)
									{
										$this->model_catalog_product->addProduct($data_new_product0);
										$number_of_new_products++;
									}
								}
							}
						}
						$seconds_rows = true;
					}
					fclose($handle);
					unlink($target_path);

					$this->data['observations']  = sprintf($this->language->get('number_of_product_imported'), $number_of_cvs_products,$number_of_new_products,$number_of_update_products,$observations0) . $linecount;
				}
				else
				{
				     $this->data['observations'] = $this->language->get('error_upload_cvs');
				}
				}

			$this->session->data['success'] = $this->language->get('text_success') . $this->data['observations'];

			$this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');

		$this->data['text_choose_file_upload'] = $this->language->get('text_choose_file_upload');
		$this->data['text_upload_file'] = $this->language->get('text_upload_file');

		$this->data['entry_version_status'] = $this->language->get('entry_version_status');
		$this->data['entry_author'] = $this->language->get('entry_author');
		$this->data['entry_help'] = $this->language->get('entry_help');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');

		$this->data['tab_general'] = $this->language->get('tab_general');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		if (isset($this->error['sort_order'])) {
			$this->data['error_sort_order'] = $this->error['sort_order'];
		} else {
			$this->data['error_sort_order'] = '';
		}
		if (isset($this->error['code'])) {
			$this->data['error_code'] = $this->error['code'];
		} else {
			$this->data['error_code'] = '';
		}

        $this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'href'      => HTTPS_SERVER .'index.php?route=common/home&token=' . $this->session->data['token'],
       		'text'      => $this->language->get('text_home'),
      		'separator' => FALSE
   		);

   		$this->data['breadcrumbs'][] = array(
       		'href'      => HTTPS_SERVER .'index.php?route=extension/module&token=' . $this->session->data['token'],
       		'text'      => $this->language->get('text_module'),
      		'separator' => ' :: '
   		);

   		$this->data['breadcrumbs'][] = array(
       		'href'      => HTTPS_SERVER .'index.php?route=module/products_importer&token=' . $this->session->data['token'],
       		'text'      => $this->language->get('heading_title'),
      		'separator' => ' :: '
   		);

		$this->data['action'] = $this->url->link('module/products_importer', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		$this->data['export_csv'] = $this->url->link('module/products_importer/download', 'token=' . $this->session->data['token'], 'SSL');
		//$this->data['export_csv'] = $this->data['export_csv'] . '&attribute1=attribute1value&&attribute2=attribute2value';

		$this->data['modules'] = array();

		if (isset($this->request->post['products_importer_module'])) {
			$this->data['modules'] = $this->request->post['products_importer_module'];
		} elseif ($this->config->get('products_importer_module')) {
			$this->data['modules'] = $this->config->get('products_importer_module');
		}


        $this->load->model('design/layout');

		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'module/products_importer.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

        $this->response->setOutput($this->render());
	}

	private function validate() {

		if (!$this->user->hasPermission('modify', 'module/products_importer')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function download() {

		//if (($this->request->server['REQUEST_METHOD'] == 'GET') && ($this->validate())){
		if ($this->validate()) {

			// set appropriate memory and timeout limits
			//ini_set("memory_limit","128M");
			//set_time_limit( 1800 );

			// send the categories, products and options as a spreadsheet file
			//$this->load->model('tool/export');
			//$this->model_tool_export->download();

			$csv_file = 'name,model,price,quantity,image_url,product_description,sku,upc,weight,category_name,attribute1,attribute2,';
			//$csv_file = 'product_url,name,model,price,quantity,image_url,product_description,sku,upc,weight,category_name';

			$attribute1 = '';

			if (isset($this->request->get['attribute1']))
				 $attribute1 = $this->request->get['attribute1'];

			$attribute2 = '';
			if (isset($this->request->get['attribute2']))
				 $attribute2 = $this->request->get['attribute2'];

			$query = $this->db->query("SELECT " . DB_PREFIX . "product.product_id,name,model,price,quantity,sku,upc,image,width,weight,description,IFNULL((SELECT name FROM " . DB_PREFIX . "category_description WHERE " . DB_PREFIX . "category_description.category_id=" . DB_PREFIX . "product_to_category.category_id LIMIT 1),'') category, (SELECT IFNULL(" . DB_PREFIX . "product_attribute.text,'') as attribute1 FROM " . DB_PREFIX . "attribute_description," . DB_PREFIX . "product_attribute WHERE " . DB_PREFIX . "product_attribute.attribute_id=" . DB_PREFIX . "attribute_description.attribute_id AND " . DB_PREFIX . "attribute_description.name='" . mysql_real_escape_string($attribute1) . "' AND " . DB_PREFIX . "product_attribute.product_id=" . DB_PREFIX . "product.product_id UNION SELECT '' LIMIT 1) attribute1,(SELECT IFNULL(" . DB_PREFIX . "product_attribute.text,'') as attribute1 FROM " . DB_PREFIX . "attribute_description," . DB_PREFIX . "product_attribute WHERE " . DB_PREFIX . "product_attribute.attribute_id=" . DB_PREFIX . "attribute_description.attribute_id AND " . DB_PREFIX . "attribute_description.name='" . mysql_real_escape_string($attribute2) . "' AND " . DB_PREFIX . "product_attribute.product_id=" . DB_PREFIX . "product.product_id UNION SELECT '' LIMIT 1) attribute2 FROM " . DB_PREFIX . "product," . DB_PREFIX . "product_description," . DB_PREFIX . "product_to_category WHERE " . DB_PREFIX . "product.product_id=" . DB_PREFIX . "product_description.product_id AND " . DB_PREFIX . "product_to_category.product_id=" . DB_PREFIX . "product.product_id ORDER BY name ASC;");
			foreach ($query->rows as $result) {
				$csv_file =sprintf("%s\n%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,",$csv_file,$this->download_field_csv_format($result['name']),$this->download_field_csv_format($result['model']),$result['price'],$result['quantity'],$this->download_image_url_format($result['image']),$this->download_field_csv_format($result['description']),$this->download_field_csv_format($result['sku']),$this->download_field_csv_format($result['upc']),$result['weight'],$this->download_field_csv_format($result['category']),$this->download_field_csv_format($result['attribute1']),$this->download_field_csv_format($result['attribute2']));
				//$csv_file =sprintf("%s\n%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s",$csv_file,$this->product_url($result['product_id']),$result['name'],$result['model'],$result['price'],$result['quantity'],$this->download_image_url_format($result['image']),$this->download_field_csv_format($result['description']),$result['sku'],$result['upc'],$result['weight'],$result['category']);

			}
			$this->response->addHeader('Content-Type: text/txt;');
			$this->response->addHeader(sprintf('Content-Disposition: attachment; filename=cvsfile%s.txt',date("Ymd")));
			$this->response->setOutput($csv_file);

		} else {

			// return a permission error page
			return $this->forward('error/permission');
		}
	}
	protected  function product_url($product_id0) {

		return sprintf("%sindex.php?route=product/product&product_id=%s",str_replace('admin/index.php?route=','',$this->url->link('')),$product_id0);

	}
	protected  function download_image_url_format($image_url0) {

		return sprintf("%simage/%s",str_replace('admin/index.php?route=','',$this->url->link('')),$image_url0);

	}
	protected  function download_field_csv_format($field0) {
		return trim(preg_replace('/[ ]{2,}|[\t]/', ' ', str_replace("\"",'',str_replace("\\",'',str_replace(",",'',str_replace("\r",'',str_replace("\n",'',$field0)))))));
	}

	//http://www.realisingdesigns.com/2011/05/16/add-seo-slugs-to-opencart-products/
	// Based on... Author: Salman Javaid   http://www.adminhelpline.com/en/free-scripts/php/string/create-slug-with-php
	protected function generateSlug($phrase) {
	    $result = strtolower($phrase);
	    $result = str_replace(array(' ','/', '_', '+', '.', '&amp;', '&'), '-', $result);
	    $result = str_replace(array('�','�', '�'), 'e', $result);
	    $result = preg_replace("/[^a-z0-9\-]/", "", $result);
	    $result = trim(substr($result, 0, 45));
	    return $result;
	}


}
?>