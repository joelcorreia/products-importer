<?php
#############################################################################
#	Version 1.2014.08.13
#  	Original developed to OpenCart 1.5.x by  Joel Correia, web: www.joelcorreia.com email: email@joelcorreia.com
#
#############################################################################
// Heading
$_['heading_title']    = 'Product Importer, Exporter AND Sincronization by CSV File';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have used module products_importer! ';
$_['text_none'] = 'None';
$_['default_category_title'] = 'Use properties of Categorie';
$_['default_tax_title'] = 'Use properties of  Tax Rate';
$_['default_product_title'] = 'Use properties of  Product';
$_['number_of_product_imported'] = 'Number of products imported: %d New products added: %d Products information updated: %d %s';
$_['default_image_download_title'] = 'Method used to download images: allow_url_fopen or curl';
$_['update_products_if_model_text_equal'] = 'Update Product fields if already exists in OpenCart';
$_['update_products_if_model_text_equal'] = 'Update Product fields if already exists in OpenCart';
$_['insert_products_if_new'] = 'Create New Product if not exists in OpenCart';
$_['time_execution_limit'] = 'Execution time limit in seconds';


$_['text_choose_file_upload'] = 'Choose a file to upload:';
$_['text_upload_file'] = 'Import or sincronize products';

$_['text_csv_file_example_link'] = 'http://www.joelcorreia.pt/product_importer/example.csv';
$_['text_csv_head_example'] = 'name,model,price,quantity,image_url,product_description,sku,upc,weight,category_name,attribute1,attribute2 <span style="color: GREEN;">(Must be on this exact order)</span>';
$_['text_csv_row_example'] = 'usb pen,usb pen,7,400,http://www.joelcorreia.pt/product_importer/pen.jpg,<b>usb pen 5 gb</b>,0002,0002,0.2,PC,text attribute 1,text attribute 2,';
$_['text_csv_example_title'] = 'Example of csv file:';

$_['text_product_row_options'] = 'Product/Row Options';
$_['text_compare_product_field'] = 'Identifier field for existing products:';

$_['text_settings'] = 'Settings';
$_['text_delimiter'] = 'delimiter (must be a single character)';
$_['text_enclosure'] = 'enclosure (must be a single character)';
$_['text_escape'] = 'escape (must be a single character)';
$_['text_execute'] = 'Execute';
$_['text_force_utf'] = 'Force UTF';


$_['text_import_or_sincronize_title'] = 'Import or Sincronize';
$_['text_export_title'] = 'Export';
$_['text_download_csv_title'] = 'Download CSV';
$_['text_download_csv_observations'] = 'Take atention with some characters that might be equal to the delimiter, enclosure or escape char';

$_['text_product_row_attributes'] = 'Product Attribute Name';
$_['text_product_row_attribute1_title'] = 'Attribute 1';
$_['text_product_row_attribute2_title'] = 'Attribute 2';


// Entry
$_['entry_version_status']= 'Developed for Opencart 1.5.x ATENTION IMPORTANT: <span style="color: RED;">Before using the importer please make a database backup.</span>';
$_['entry_author']	      = 'Developed for Opencart 1.5.x by <a href="http://www.joelcorreia.com/">http://www.joelcorreia.com/.';
$_['entry_help']	      = '';



// Error
$_['error_permission']  = 'Warning: You do not have permission to modify module products_importer!';
$_['error_sort_order'] 	= 'Please enter position!';
$_['error_code'] 		= 'Please enter code of products_importer!';
$_['error_upload_cvs'] 		= 'There was an error uploading the file, please try again!';



?>